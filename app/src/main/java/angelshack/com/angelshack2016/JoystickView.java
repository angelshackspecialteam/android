package angelshack.com.angelshack2016;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Duffman on 21/5/16.
 */
public class JoystickView extends View {
    private final static String TAG="JoystickView";
    private static final int MAX_X = 1024;
    private static final int MAX_Y = 1024;

    private int x=-1;
    private int y=-1;
    private int radius = 5;
    private Paint paint = new Paint();


    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(x==-1 && y==-1) {
            x = getWidth() / 2;
            y = getHeight() / 2;
        }

        canvas.drawCircle(x, y, radius, paint);
    }

    public void setX(int xP) {
        x = xP * getWidth()/MAX_X;
    }

    public void setY(int yP) {
        y = yP * getHeight()/MAX_Y;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}
