package angelshack.com.bluetoothlibrary;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Duffman on 21/5/16.
 */
public class ArduinoBluetoothAdapter implements IBtBaseAdapter{
    private final static String TAG="ArduinoBluetoothAdapter";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final int REQUEST_BLUETOOTH = 0;
    private static final String HC06_DEVICE_NAME="HC-06";
    private Reciver reciver;
    private String[] data = {"514","514","0","0","0","0","0","0"};
    private Random random;

    public static ArduinoBluetoothAdapter initialize(){
        return new ArduinoBluetoothAdapter();
    }

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private boolean connected=false;

    //private boolean btnAxis, btn1, btn2, btn3, btn4, viber;
    //private String xAxis, yAxis, xAccel, yAccel, zAccel, xGyro, yGyro, zGyro;

    private ArduinoBluetoothAdapter(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        random = new Random();
        if (bluetoothAdapter == null) {
            Log.e(TAG,"Device doesnt Support Bluetooth");
        } else {
            if (!bluetoothAdapter.isEnabled()){
                Log.e(TAG,"Bluetooth no enabled");
            }
        }
    }

    @Override
    public void connectHC06() {
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();

        for(BluetoothDevice device : bondedDevices){
            Log.d(TAG,"device name: "+device.getName());
            if(device.getName().equals(HC06_DEVICE_NAME)){
                connectToDevice(device);
                return;
            }
        }
        throw new RuntimeException("device "+HC06_DEVICE_NAME+" not found");
    }
    private void connectToDevice(BluetoothDevice device) {
        try {
            socket = device.createRfcommSocketToServiceRecord(MY_UUID);
            socket.connect();
            outputStream=socket.getOutputStream();
            inputStream=socket.getInputStream();

            reciver  =new Reciver();
            reciver.execute();
            connected=true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public void disconnectHC06() {
        if(isConnected()){
            try {
                reciver.stop();
                inputStream.close();
                outputStream.close();
                socket.close();

                connected = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void viber() {
        try {
            outputStream.write("1".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean button1Pressed() {
        return !data[4].equals("0");
    }

    @Override
    public boolean button2Pressed() {
        return !data[5].equals("0");
    }

    @Override
    public boolean button3Pressed() {
        return !data[6].equals("0");
    }

    @Override
    public boolean button4Pressed() {
        return !data[7].equals("0");
    }

    @Override
    public boolean buttonJoystickPressed() {
        return !data[3].equals("0");
    }

    @Override
    public float[] getAccX() {
        float[] response = new float[60];
        for(int i = 0; i < 60; ++i){
            response[i] = random.nextFloat() * 2 -1;
        }
        return response;
    }

    @Override
    public float[] getAccY() {
        float[] response = new float[60];
        for(int i = 0; i < 60; ++i){
            response[i] = random.nextFloat() * 2 -1;
        }
        return response;
    }

    @Override
    public float[] getAccZ() {
        float[] response = new float[60];
        for(int i = 0; i < 60; ++i){
            response[i] = random.nextFloat() * 2 -1;
        }
        return response;
    }

    @Override
    public int getXJoystick() {
        try {
            return Integer.valueOf(data[0]);
        }catch (Exception e){
            Log.e(TAG,"error parse int. value: "+data[0]);
            return 514;
        }
    }

    @Override
    public int getYJoystick() {
        try {
            return Integer.valueOf(data[1]);
        }catch (Exception e){
            Log.e(TAG,"error parse int. value: "+data[1]);
            return 514;
        }
    }

    @Override
    public boolean getViber() {
        return !data[2].equals("0");
    }

    private void updateValues(String newValue){
         String[]split = newValue.split(",");
        if(split.length==8) data=split;
    }

    private class Reciver extends AsyncTask<Void,Void,Void> {

        private boolean stop;
        private String tmp="";

        public Reciver(){
            stop=false;
        }

        @Override
        protected Void doInBackground(Void... params) {
            while(!stop) {
                try {

                    int byteCount = inputStream.available();
                    byte[] rawBytes = new byte[byteCount];
                    if (byteCount > 0) {
                        final String string;
                    //Log.d(TAG,"pre read");
                        inputStream.read(rawBytes);
                    //Log.d(TAG,"post read");
                        string = new String(rawBytes, "UTF-8");
                        tmp +=string;
                        //Log.d(TAG,tmp);
                        if(tmp.contains("\r\n")){
                            String[] split = tmp.split("\r\n");

                            if (split.length==0){
                            } else if(split.length==1){
                                updateValues(split[0]);
                                tmp="";
                            }else {
                                for (int i = 0; i < split.length - 1; ++i) {
                                    updateValues(split[i]);
                                }
                                tmp = split[split.length - 1];
                            }
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;

        }

        public void stop(){
            stop=true;
        }
    }
}
